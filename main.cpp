#include <vector>
#include <cstdlib>
#include <pcap.h>
#include "mac.h"
#include <iostream>
#include <string>
#include <pcap.h>
#include <sys/socket.h>
#include <unistd.h>
#include <cstring>
#include <algorithm>
#include <set>
#include <utility>

using namespace std;
typedef long long int ll;
typedef long double dl;
typedef pair<dl,dl> pdi;
typedef pair<ll,ll> pii;
typedef pair<ll,pii> piii;

#define ff first
#define ss second
#define eb emplace_back
#define ep emplace
#define pb push_back
#define mp make_pair
#define all(x) (x).begin(), (x).end()
#define compress(v) sort(all(v)), v.erase(unique(all(v)), v.end())
#define IDX(v, x) lower_bound(all(v), x) - v.begin()
//cout<<fixed;
//cout.precision(12);

#define ESSID_LEN 32
#define ETHER_ADDR_LEN 6

struct ieee80211_radiotap_header {
        u_int8_t        it_version;     /* set to 0 */
        u_int8_t        it_pad;
        u_int16_t       it_len;         /* entire length */
        u_int32_t       it_present;     /* fields present */
} __attribute__((__packed__));
typedef ieee80211_radiotap_header rthdr;

struct ieee80211_mac_header
{
	u_int16_t frame_control;
	u_int16_t id;
	Mac dmac;
	Mac smac;
	Mac bss;
	u_int16_t seq;
};
typedef ieee80211_mac_header machdr;

struct Wireless
{
    u_int8_t not_use[13];
    u_int8_t ssid_len;
}__attribute__((__packed__));
typedef Wireless wireless;

struct line
{
	Mac bssid;
	int beacons;
	char essid[ESSID_LEN];
};

void usage()
{
	printf("syntax : sudo ./airodump <interface>\n");
	printf("sample : sudo ./airodump mon0\n");
}


void print_xxd(u_char* arr, int n)
{
	int i,j,k;
	for(i=0;i<n;i++)
		printf("%02x ", arr[i]);	
	cout<<endl;
}

void print_Mac(u_int8_t* mac_arr)
{
	int i,j,k;
	for(i=0;i<6;i++){
		printf("%02x", mac_arr[i]);
		if(i==5)
			cout<<endl;
		else cout<<':';
	}
}

void printlines()
{
	system("clear");
	printf("BSS\t\t\tbeacons\tESSID\n");
	for (auto k : line_store){
    	printMac((u_int8_t*)k.bssid);
		printf("\t%d\t%s\n", k.beacons, k.essid);
    }
}

int main(int argc, char* argv[]) {
	if(argc != 2){
		usage();
		return 0;
	}

	char *dev = argv[1];
	char errbuf[PCAP_ERRBUF_SIZE];
	pcap_t* pcap = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);
	if (pcap == NULL) {
		cout << "pcap_open_live( "<< dev << ") return null - " << errbuf << endl;
		return -1;
	}

	struct pcap_pkthdr* header;
	const u_char* packet = (u_char*)malloc(BUFSIZ);
	if(packet == NULL){
		cout<< "malloc return null" <<endl;
		return -1;
	}

	while (1) {
		int pcap_res = pcap_next_ex(pcap, &header, &packet);
		if (pcap_res == 0) continue;
		if (pcap_res == PCAP_ERROR || res == PCAP_ERROR_BREAK) {
			cout<<"pcap_next_ex return " << pcap_res << "(" << pcap_geterr(pcap) << endl;
			break;
		}

		rthdr* rtap = (rthdr*)packet;
    	machdr* beacon = (machdr*)(packet + rtap->it_len);
    	wireless* wless = (wireless*)(packet + rtap->it_len + sizeof(machdr));
    	char* ssid = (char*)(packet + rtap->it_len + sizeof(machdr) + sizeof(wireless));
    	
	}
	free((u_char*)packet);
	pcap_close(pcap);
	return 0;
}

